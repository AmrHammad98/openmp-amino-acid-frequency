#include "stdafx.h"
#include "Sections.h"
#include <iostream>

extern map<char, int> index;																// mapping each letter to their index 
extern string codonTable[4][4][4];															// 3D array representing codon table 

// a function to count number of acids using sections of Open-mp
// parameters: input -> input string (sequence from input file), NTHREADS -> number of threads to be used 
// returns a map of amino acids mapped to their frequencies
map<string, int> countAcids_Sections(string input)
{
	int sum = 0;
	string triplet = "";										// string representing a triplet 

	// maping each amino acid to its frequency to be returned by the count rep function when frequencies are calculated 
	map<string, int>res = { { "phe", 0 },{ "leu", 0 },{ "iie", 0 },{ "met", 0 },
	{ "val", 0 },{ "ser", 0 },{ "pro", 0 },{ "thr", 0 },
	{ "ala", 0 },{ "tyr", 0 },{ "his", 0 },{ "gin", 0 },
	{ "asn", 0 },{ "lys", 0 },{ "asp", 0 },{ "glu", 0 },
	{ "cys", 0 },{ "stop", 0 },{ "trp", 0 },{ "arg", 0 }
	,{ "gly", 0 } };

	map<string, int>private_res = { { "phe", 0 },{ "leu", 0 },{ "iie", 0 },{ "met", 0 },
	{ "val", 0 },{ "ser", 0 },{ "pro", 0 },{ "thr", 0 },
	{ "ala", 0 },{ "tyr", 0 },{ "his", 0 },{ "gin", 0 },
	{ "asn", 0 },{ "lys", 0 },{ "asp", 0 },{ "glu", 0 },
	{ "cys", 0 },{ "stop", 0 },{ "trp", 0 },{ "arg", 0 }
	,{ "gly", 0 } };

	int lower, upper;

	#pragma omp parallel shared(res)
	{
		#pragma omp sections nowait private(private_res, lower, upper)
		{
			#pragma omp section
			{
				lower = omp_get_thread_num() * (input.size() / 2);
				upper = (omp_get_thread_num() + 1) * (input.size() / 2);
				private_res = sec( input,lower ,upper);

				map<string, int>::iterator it = private_res.begin();										// iterator to itterate over rep map 
				#pragma omp critical
				{
					for (it; it != private_res.end(); ++it)
					{
						res.at(it->first) += it->second;
					}
				}
			}
			#pragma omp section
			{
				lower = omp_get_thread_num() * (input.size() / 2);
				upper = (omp_get_thread_num() + 1) * (input.size() / 2);
				private_res = sec( input,lower ,upper);

				map<string, int>::iterator it = private_res.begin();										// iterator to itterate over rep map 
				#pragma omp critical
				{
					for (it; it != private_res.end(); ++it)
					{
						res.at(it->first) += it->second;
					}
				}
			}
		}
	}
	return res;
}

map<string, int> sec(string input, int lower, int upper)
{
	map<string, int>mp = { { "phe", 0 },{ "leu", 0 },{ "iie", 0 },{ "met", 0 },
	{ "val", 0 },{ "ser", 0 },{ "pro", 0 },{ "thr", 0 },
	{ "ala", 0 },{ "tyr", 0 },{ "his", 0 },{ "gin", 0 },
	{ "asn", 0 },{ "lys", 0 },{ "asp", 0 },{ "glu", 0 },
	{ "cys", 0 },{ "stop", 0 },{ "trp", 0 },{ "arg", 0 }
	,{ "gly", 0 } };

	string triplet = "";
	int first, second, third;
	for (int i = lower; i < upper; i++)
	{
		if (triplet.length() == 3)
		{
			first = index.at(triplet.at(0));
			second = index.at(triplet.at(1));
			third = index.at(triplet.at(2));

			mp.at(codonTable[first][second][third])++;

			triplet.clear();
			triplet.push_back(input[i]);
		}
		else
		{
			triplet.push_back(input[i]);
		}
	}
	return mp;
}
