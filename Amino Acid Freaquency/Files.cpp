#include "stdafx.h"
#include "Files.h"

// a function to read an input sequence file
// parameters: file name -> name of file to read from 
// returns a string of what has been read from input file 
string readFile(string fileName)
{
	// create file object 
	ifstream ifs(fileName);

	string sequence;											// a string representing DNA sequence obtained from input file 
	char c;														// char to be read from file 

	// check if stram is open before reading
	if (ifs.is_open())
	{
		// loop until  EOF 
		while (!(ifs.eof()))
		{
			// get char 
			ifs.get(c);

			// skip if c is a digit, space, newline or '/'
			if ( c == '/' || c == '\n' || c == ' '|| isdigit(c))
			{
				continue;
			}
			else
			{
				// append string with char 
				sequence.push_back(c);
			}
		}
	}
	return sequence;
}