// Amino Acid Freaquency.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Files.h"
#include "Serial.h"
#include "Sections.h"
#include "Parallel_Loops.h"
#include <omp.h>
#include <ctime>

#define NTHREADS 16

using namespace std;

// prototypes 
void printMap(map<string, int> mp, int size, clock_t clock1, clock_t clock2);

int main()
{
	string fn = "InputSeq.txt";															// file name 
	string input = readFile(fn);														// string from input file 

	// Serial 
	/*cout << "Serial" << endl;
	clock_t time = clock();
	map<string, int> Serial = countAcids(input);
	printMap(Serial, input.length(), time, clock());*/

	cout << endl;

	// Loop level parallelism
	cout << "Loop Level parallelism" << endl;
	clock_t time = clock();
	map<string, int>Loops = countAcids_pLoops(input, NTHREADS);
	printMap(Loops, input.length(), time, clock());

	cout << endl;

	// Sections
	/*cout << "Sections" << endl;
	clock_t time = clock();
	map<string, int>Sections = countAcids_Sections(input);
	printMap(Sections, input.length(), time, clock());*/

    return 0;
}


// a function to print a map
void printMap(map<string, int> mp, int size, clock_t clock1, clock_t clock2)
{
	map<string, int>::iterator it = mp.begin();										// iterator to itterate over rep map 
	for (it; it != mp.end(); ++it)
	{
		for (int i = 0; i < it->first.length(); i++)
		{
			cout << it->first[i];
		}
		cout << ":" << it->second << endl;
		cout << " Percentage: " << ((float)it->second / size / 3) * 100 << "%" << endl;
	}

	cout << "Time Taken: " << (float)(clock2 - clock1) / CLOCKS_PER_SEC << " Seconds" << endl;
}