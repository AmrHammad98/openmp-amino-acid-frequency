#include "stdafx.h"
#include "Serial.h"

extern map<char, int> index;																// mapping each letter to their index 
extern string codonTable[4][4][4];															// 3D array representing codon table 

// a function to divide input string into equal chunks and count frequencies for amino acids 
// parameters: input -> input string (sequence from input file)
// returns a map of amino acids mapped to their frequencies 
map<string, int> countAcids(string input)
{
	string triplet = "";																	// string representing a triplet  
	int first, second, third;																// indices: first -> first letter in codon table, second -> second letter.....

    // maping each amino acid to its frequency to be returned by the count rep function when frequencies are calculated 
	map<string, int>res = { { "phe", 0 },{ "leu", 0 },{ "iie", 0 },{ "met", 0 },
	{ "val", 0 },{ "ser", 0 },{ "pro", 0 },{ "thr", 0 },
	{ "ala", 0 },{ "tyr", 0 },{ "his", 0 },{ "gin", 0 },
	{ "asn", 0 },{ "lys", 0 },{ "asp", 0 },{ "glu", 0 },
	{ "cys", 0 },{ "stop", 0 },{ "trp", 0 },{ "arg", 0 }
	,{ "gly", 0 } };
 
	for (int i = 0; i < input.length(); i++) 
	{
		// triplet is formed 
		if (triplet.size() == 3)
		{
			// indexes to be accessed from codon table 
			first = index.at(triplet.at(0));
			second = index.at(triplet.at(1));
			third = index.at(triplet.at(2));

			// update map 
			res.at(codonTable[first][second][third])++;

			// clear triplet for the next one 
			triplet.clear();
			// append character fo the next triplet 
			triplet.push_back(input.at(i));
		}
		else
		{
			// append triplet with current char
			triplet.push_back(input.at(i));
		}
	}

	return res; 
}
