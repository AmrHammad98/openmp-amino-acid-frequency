// This file contains all constants to be used in this project
#include "stdafx.h"
#include <map>

using namespace std;

// maps 
map<char, int> index = { { 't', 0 },{ 'c', 1 },{ 'a', 2 },{ 'g', 3 } };						// mapping each char to its index in codon table

// codon table 
string codonTable[4][4][4] =
{
	{ { "phe","phe","leu","leu" },{ "ser","ser","ser","ser" },{ "tyr","tyr" ,"stop","stop" },{ "cys","cys","stop","trp" } },
{ { "leu" ,"leu","leu","leu" },{ "pro","pro","pro","pro" },{ "his","his","gin","gin" },{ "arg","arg","arg","arg" } },
{ { "iie","iie","iie","met" },{ "thr","thr","thr","thr" },{ "asn","asn","lys","lys" },{ "ser","ser","arg","arg" } },
{ { "val","val","val","val" },{ "ala","ala","ala","ala" },{ "asp","asp","glu","glu" },{ "gly","gly","gly","gly" } }
};