#include "stdafx.h"
#include "Parallel_Loops.h"

extern map<char, int> index;																// mapping each letter to their index 
extern string codonTable[4][4][4];															// 3D array representing codon table 

// a function to count acids using Open-mp loop-level parallelism
// parameters: input -> input string (sequence from input file), NTHREADS -> number of threads to be used 
// returns a map of amino acids mapped to their frequencies
map<string, int> countAcids_pLoops(string input, int NTHREADS)
{
	string triplet = "";										// string representing a triplet 
	unsigned int chunk = input.size() / NTHREADS;

	// maping each amino acid to its frequency to be returned by the count rep function when frequencies are calculated 
	map<string, int>res = { { "phe", 0 },{ "leu", 0 },{ "iie", 0 },{ "met", 0 },
	{ "val", 0 },{ "ser", 0 },{ "pro", 0 },{ "thr", 0 },
	{ "ala", 0 },{ "tyr", 0 },{ "his", 0 },{ "gin", 0 },
	{ "asn", 0 },{ "lys", 0 },{ "asp", 0 },{ "glu", 0 },
	{ "cys", 0 },{ "stop", 0 },{ "trp", 0 },{ "arg", 0 }
	,{ "gly", 0 } };

	// set number of threads 
	omp_set_num_threads(NTHREADS);

	int r, t, c;

	// start parallelising
	#pragma omp parallel for schedule(static, chunk) shared(res) private(triplet, r, t, c)
	for (int i = 0; i < input.size(); i++)
	{
		if (triplet.size() == 3)
		{
			r = index.at(triplet.at(0));
			t = index.at(triplet.at(1));
			c = index.at(triplet.at(2));

			#pragma omp critical
			{
				res.at(codonTable[r][t][c])++;
			}

			triplet.clear();
			triplet.push_back(input.at(i));
		}
		else
		{
			triplet.push_back(input.at(i));
		}
	}
	return res;
}
